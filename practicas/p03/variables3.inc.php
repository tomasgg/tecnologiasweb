    <?php
    unset($a, $b, $c);
    $a = "PHP5";
    echo ("
    <div>
        <p>Variable \$a: $a<br></p>
    </div>");

    $z[] = &$a;
    echo ("
    <div>
        <p>Arreglo \$z: <pre>");
    print_r($z);
    echo ("</pre></p>
    </div>");

    $b = "5a version de PHP";
    echo ("
    <div>
        <p>Variable \$b: $b<br></p>
    </div>");

    $c = $b * 10;
    echo ("
    <div>
        <p>Variable \$c: $c<br></p>
    </div>");

    $a .= $b;
    echo ("
    <div>
        <p>Variable \$a: $a<br></p>
    </div>");

    $b *= $c;
    echo ("
    <div>
        <p>Variable \$b: $b<br></p>
    </div>");

    $z[0] = "MySQL";
    echo ("
    <div>
        <p>Arreglo \$z: <pre>");
    print_r($z);
    echo ("</pre></p>
    </div>");
    ?>