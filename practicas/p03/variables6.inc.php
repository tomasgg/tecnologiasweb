    <?php
    unset($a, $b, $c, $d, $e, $f);
    $a = "0";
    $b = "TRUE";
    $c = FALSE;
    $d = ($a or $b);
    $e = ($a and $c);
    $f = ($a xor $b);

    echo ("
<div>
    <p>Variable \$a: ");
    var_dump($a);
    echo ("<br>Valor booleano: ");
    var_dump(boolval($a));
    echo ("</p>
</div>"
    );

    echo ("
<div>
    <p>Variable \$b: ");
    var_dump($b);
    echo ("<br>Valor booleano: ");
    var_dump(boolval($b));
    echo ("</p>
</div>"
    );

    echo ("
<div>
    <p>Variable \$c: ");
    var_dump($c);
    echo ("<br>Valor booleano: ");
    var_dump(boolval($c));
    echo ("</p>
</div>"
    );

    echo ("
<div>
    <p>Variable \$d: ");
    var_dump($d);
    echo ("<br>Valor booleano: ");
    var_dump(boolval($d));
    echo ("</p>
</div>"
    );

    echo ("
<div>
    <p>Variable \$e: ");
    var_dump($e);
    echo ("<br>Valor booleano: ");
    var_dump(boolval($e));
    echo ("</p>
</div>"
    );

    echo ("
<div>
    <p>Variable \$f: ");
    var_dump($f);
    echo ("<br>Valor booleano: ");
    var_dump(boolval($f));
    echo ("</p>
</div>"
    );

    function printbool($bool)
    {
        if ($bool) {
            return ("TRUE");
        } else {
            return ("FALSE");
        }
    }
    echo ("
<div>
    <p>Variables \$c y \$e mostrados con echo <br>\$c: " . printbool($c) . "<br>\$e: " . printbool($e) . "</p>
</div>
");
    ?>